const crypto = require('crypto')

const pcl = require('postchain-client')
const secp256k1 = require('secp256k1/elliptic')

let privKey
do {
    privKey = crypto.randomBytes(32)
} while (!secp256k1.privateKeyVerify(privKey))
const hexPrivKey = privKey.toString('hex')

const hexPubKey = secp256k1.publicKeyCreate(privKey).toString('hex')


const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// default blockchain identifier used for testing
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";

const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5)

const user = pcl.util.makeKeyPair()
test_account_create()

function test_account_create(chainfunction){




	const gtx = pcl.gtxClient.createClient(
		rest,
		Buffer.from(blockchainRID, 'hex'),
		["create_account"]
	);
    const user = pcl.util.makeKeyPair();
    const tx = gtx.newTransaction([user.pubKey]);
	
	tx.addOperation('create_account', user.pubKey);

	// Sign transaction
	tx.sign(user.privKey, user.pubKey);

	// Commit transaction using promise
	var funcname = "create_account"; // For console logging in success/error
	console.log(funcname + " Execute operation and wait for result");
	return tx.postAndWaitConfirmation()
		// Success
		.then(function(result){
			console.log(funcname + " Success:");
			console.log(result);
			console.log("-------------------");
			if(chainfunction !== undefined){
				chainfunction();
			}
		})
		// Error
		.catch(function(err){
			console.log(funcname + " Error:");
			console.log(err);
			console.log("-------------------");
			if(chainfunction !== undefined){
				chainfunction();
			}			
		})
	;
}