const users = require('./users')
const pcl = require('postchain-client')
const node_api_url = "http://ide.chromiadev.net/node/10010/"; // using default postchain node REST API port

// default blockchain identifier used for testing
// const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";


const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5)

test_accounts_create()

function test_accounts_create(chainfunction) {
    for (const user of users) {
        console.log('IN LOOP')

        const gtx = pcl.gtxClient.createClient(
            rest,
            Buffer.from(blockchainRID, 'hex'),
            ["create_account"]
        );
        const tx = gtx.newTransaction([user.pubKey]);

        tx.addOperation('create_account', user.pubKey, user.name);

        // Sign transaction
        tx.sign(user.privKey, user.pubKey);

        // Commit transaction using promise
        var funcname = "create_account"; // For console logging in success/error
        console.log(funcname + " Execute operation and wait for result");
        tx.postAndWaitConfirmation()
            // Success
            .then(function (result) {
                console.log(funcname + " Success:");
                console.log(result);
                console.log("-------------------");
                if (chainfunction !== undefined) {
                    chainfunction();
                }
            })
            // Error
            .catch(function (err) {
                console.log(funcname + " Error:");
                console.log(err);
                console.log("-------------------");
                if (chainfunction !== undefined) {
                    chainfunction();
                }
            });
    }
}