const users = require('./users')
const crypto = require("crypto")


const pcl = require('postchain-client')

const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// default blockchain identifier used for testing
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";

const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5)

test_join_escrows()



function theOtherUser(forThisUser){
    var found = false
    const firstUser = users[users.length -1]
    for (user of users.reverse()){
        if (found){
            return user
        }
        if (user.pubKey === forThisUser.pubKey) {
            found = true
        }
    }
    return firstUser
}

// accept(escrow_id:text, seller_pk:pubkey)
function test_join_escrows(chainfunction) {
    for (const user of users) {
        console.log('IN LOOP')
        const gtx = pcl.gtxClient.createClient(
            rest,
            Buffer.from(blockchainRID, 'hex'),
            ["accept"]
        );
        const tx = gtx.newTransaction([user.pubKey]);
        const buyer = theOtherUser(user)

        tx.addOperation('accept', buyer.escrowId,
            user.pubKey);

        // price: integer, escrow_id:text, seller_pk:pubkey, 
        // buyer_pk:pubkey, seller_escrow: integer, buyer_escrow:integer, escrow_text:text


        // Sign transaction
        tx.sign(user.privKey, user.pubKey);

        // Commit transaction using promise
        var funcname = "create escrow"; // For console logging in success/error
        console.log(funcname + " Execute operation and wait for result");
        tx.postAndWaitConfirmation()
            // Success
            .then(function (result) {
                console.log(funcname + " Success:");
                console.log(result);
                console.log("-------------------");
                if (chainfunction !== undefined) {
                    chainfunction();
                }
            })
            // Error
            .catch(function (err) {
                console.log(funcname + " Error:");
                console.log(err);
                console.log("-------------------");
                if (chainfunction !== undefined) {
                    chainfunction();
                }
            });
    }
}