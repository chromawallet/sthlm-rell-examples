class accounts{
  key pubkey;
  name;
  mutable balance: integer = 20000; // starting capital :)
}

operation create_account(user_pk: pubkey, name:text){
    require( is_signer(user_pk) );
    create accounts(pubkey = user_pk, name = name);
}

enum escrow_status {
    active,
    resolved,
    burned
}

class escrows {
  key id: text;
  buyer_pk: pubkey;
  seller_pk: pubkey;
  text;
  price: integer;
  buyer_escrow: integer;
  seller_escrow_size: integer;
  mutable seller_escrow: integer = 0;
  mutable status: escrow_status = escrow_status.active;
}

operation bid(price: integer, escrow_id:text, seller_pk:pubkey, 
	buyer_pk:pubkey, seller_escrow: integer, buyer_escrow:integer, escrow_text:text
){
    require( is_signer(buyer_pk) );
    val buyer_total_amount = price + buyer_escrow;
    update accounts @{ .pubkey == buyer_pk, .balance > buyer_total_amount } ( balance -= buyer_total_amount  );
    create escrows(price= price, id = escrow_id, seller_pk = seller_pk,
    	   buyer_pk = buyer_pk, buyer_escrow=buyer_escrow, seller_escrow_size = seller_escrow, text = escrow_text
    );
}

operation accept(escrow_id:text, seller_pk:pubkey){
    require( is_signer(seller_pk) );
    val escrow = escrows @{ .id==escrow_id, .seller_pk == seller_pk};
    require(escrow.status == escrow_status.active);
    require(escrow.seller_escrow == 0);
    update escrows @{ .id==escrow_id, .seller_pk == seller_pk}(seller_escrow += escrow.seller_escrow_size);
    update accounts @{.pubkey == seller_pk, .balance >= escrow.seller_escrow_size}
           (balance -= (escrow.seller_escrow_size -escrow.price));
}

operation release_escrow(escrow_id:text, buyer_pk:pubkey){
    require( is_signer(buyer_pk) );
    val escrows = escrows @{ .id==escrow_id, .buyer_pk == buyer_pk};
    require(escrows.status == escrow_status.active);
    update accounts @{.pubkey == escrows.seller_pk}(balance += escrows.seller_escrow);
    update accounts @{.pubkey == escrows.buyer_pk}(balance += escrows.buyer_escrow);
    update escrows @{.id == escrow_id}(status = escrow_status.resolved);
}

operation burn(escrow_id:text, buyer_pk:pubkey){
    require( is_signer(buyer_pk) );
    val escrow = escrows @{ .id==escrow_id, .buyer_pk == buyer_pk};
    require(escrow.status == escrow_status.active);
    require(escrow.buyer_escrow > 0);
    update escrows @{.id == escrow_id}(status = escrow_status.burned);
}

query get_balance(user_pk:pubkey){
    return accounts@{.pubkey == user_pk}(.balance);
}

query get_escrows_for_seller(seller_pk:pubkey){
      return escrows@{.seller_pk == seller_pk};
}

query get_escrows_for_buyer(buyer_pk:pubkey){
      return escrows@{.buyer_pk == buyer_pk};
}

operation delete_escrow(escrow_id:text, user_pk:pubkey){
    // Can be done either by buyer or seller
    // as long as buyer has not paid into it
    require( is_signer(user_pk) );
    val escrow = escrows @{ .id==escrow_id, (.seller_pk == user_pk or .buyer_pk == user_pk )};
    require(escrow.seller_escrow == 0);
    update accounts @{.pubkey == escrow.buyer_pk}(balance +=  escrow.buyer_escrow);
    delete escrows @{ .id == escrow_id };
}