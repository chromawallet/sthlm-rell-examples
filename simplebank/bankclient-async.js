console.log("Simplebank postchain client utilizing async and await.");

const pcl = require('postchain-client');

const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// default blockchain identifier used for testing
//const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";  //changed to default RID for Eclipse env.

const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5);



const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(blockchainRID, 'hex'),
    ["open_account", "send_money", "get_balance"]
);


const user = pcl.util.makeKeyPair();
const user2 = pcl.util.makeKeyPair();

var tx = gtx.newTransaction([user.pubKey]);

function open_account_1(){

tx.addOperation('open_account', 1, "Anders Lundin",user.pubKey);
tx.sign(user.privKey, user.pubKey);
return tx.postAndWaitConfirmation();

}

function open_account_2(){

tx = gtx.newTransaction([user2.pubKey]);

tx.addOperation('open_account', 2, "Fredrik Reinfeldt",user2.pubKey);
tx.sign(user2.privKey, user2.pubKey);
return tx.postAndWaitConfirmation();

}

function send_money_from_1_to_2(){

tx = gtx.newTransaction([user.pubKey]);

tx.addOperation('send_money',2,333,user.pubKey);

tx.sign(user.privKey, user.pubKey);
tx.postAndWaitConfirmation();


}

function account_balance(account_number) {
  return gtx.query("get_balance", {account_number: account_number});
}


async function create_accounts_and_transfer_money(){

await open_account_1();
await open_account_2();

console.log ("Accounts ready.");

await send_money_from_1_to_2();

console.log ("Money sent. Waiting for confirmation.");


var bal1 = await account_balance(1);		//initial balance reading
var bal2 = await account_balance(2);

while (bal1==1000 || bal2==1000){
bal1= await account_balance(1);				//if any of balances remain at 1000, transfer not yet fully occured
bal2= await account_balance(2);				 
}


console.log ("Balance in acct. 1="+bal1);
console.log ("Balance in acct. 2="+bal2);

}

setTimeout(function() { create_accounts_and_transfer_money();} , 100); 
