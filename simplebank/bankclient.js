console.log("Postchain client for simplebank using waiting instead of async/await.");

const pcl = require('postchain-client');

const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// default blockchain identifier used for testing
//const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";  //changed to default RID for Eclipse env.

const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5);



const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(blockchainRID, 'hex'),
    ["open_account", "send_money", "get_balance"]
);


const user = pcl.util.makeKeyPair();
const user2 = pcl.util.makeKeyPair();

var tx = gtx.newTransaction([user.pubKey]);


tx.addOperation('open_account', 1, "Anders Lundin",user.pubKey);
tx.sign(user.privKey, user.pubKey);
tx.postAndWaitConfirmation();

tx = gtx.newTransaction([user2.pubKey]);

tx.addOperation('open_account', 2, "Fredrik Reinfeldt",user2.pubKey);
tx.sign(user2.privKey, user2.pubKey);
tx.postAndWaitConfirmation();


function send_money_from_1_to_2(){

tx = gtx.newTransaction([user.pubKey]);

tx.addOperation('send_money',2,333,user.pubKey);

tx.sign(user.privKey, user.pubKey);
tx.postAndWaitConfirmation();


}

function query_out(){
var queryResult=gtx.query("get_balance", {account_number: 1}); 
var queryResult2=gtx.query("get_balance", {account_number: 2}); 

console.log("balance in account 1=");
console.log(queryResult);

console.log("balance in account 2=");
console.log(queryResult2);


}

setTimeout(function() { send_money_from_1_to_2();} , 5000);  
setTimeout(function() { query_out(); }, 10000);

