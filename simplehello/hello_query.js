// Example of Chromia javascript client executing a QUERY on Chromia
//   (Getting all records from a chromia class table)
////
// For more information see he following links:
//    http://rell.chromia.com/en/master/
//    http://rell.chromia.com/en/master/eclipse/eclipse.html
//    https://jsfiddle.net/koniak/my87on5g/
//    // Example of Chromia javascript client executing a QUERY on Chromia
//   (Getting all records from a chromia class table)
////
// For more information see he following links:
//    http://rell.chromia.com/en/master/
//    http://rell.chromia.com/en/master/eclipse/eclipse.html
//    https://jsfiddle.net/koniak/my87on5g/
//    https://bitbucket.org/chromawallet/sthlm-rell-examples/src/master/
//////////////////////////////////////////////////////////////////////////////////
// This Chromia client example code is ment to be used with following Rell code
//   class hello {
//   	key speak: text;
//   }
//   
//   operation hello_add(name: text){
//   	create hello ('Hello ' + name + '!');
//   }
//   
//   query hello_list(){
//   	return hello@*{} ( hello.speak );
//   }
//   
//////////////////////////////////////////////////////////////////////////////////////

// Require Postchain client for accessing Chromia
const pcl = require('postchain-client'); // (Emulated node require)

// Define the URL to Chromia
const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// Define the Blockchain RID (this is the default RID)
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF"; 

// Generate a random key pair to use when accessing Chromia
const user = pcl.util.makeKeyPair();

// Initiate client
const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5);

// You must update the last argument of createClient
// with a list of all operations you will use.
const gtx = pcl.gtxClient.createClient(
	rest,
	Buffer.from(blockchainRID, 'hex'),
	['hello_list']
);


// Execute Query
////////////////////////////

// Execute query
gtx.query('hello_list', {})
	.then(function(result){ // Success
		console.log("--------------------------------------");
		console.log("Chromia call succeded with output:");
		console.log("--------------------------------------");
		console.log(result);
		console.log("--------------------------------------");
		console.log("Also check Chromia console in Eclipse");
		console.log("--------------------------------------");		
	})
	.catch(function(err){ // Fail
		console.log("--------------------------------------");
		console.log("Chromia call failed with output:");
		console.log("--------------------------------------");
		console.log(err);
		console.log("--------------------------------------");
		console.log("Also check Chromia console in Eclipse");
		console.log("--------------------------------------");
	});
//////////////////////////////////////////////////////////////////////////////////
// This Chromia client example code is ment to be used with following Rell code
//   class hello {
//   	key speak: text;
//   }
//   
//   operation hello_add(name: text){
//   	create hello ('Hello ' + name + '!');
//   }
//   
//   query hello_list(){
//   	return hello@*{} ( hello.speak );
//   }
//   
//////////////////////////////////////////////////////////////////////////////////////

// Require Postchain client for accessing Chromia
const pcl = require('postchain-client'); // (Emulated node require)

// Define the URL to Chromia
const node_api_url = "http://localhost:7740"; // using default postchain node REST API port

// Define the Blockchain RID (this is the default RID)
const blockchainRID = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF"; 

// Generate a random key pair to use when accessing Chromia
const user = pcl.util.makeKeyPair();

// Initiate client
const rest = pcl.restClient.createRestClient(node_api_url, blockchainRID, 5);

// You must update the last argument of createClient
// with a list of all operations you will use.
const gtx = pcl.gtxClient.createClient(
	rest,
	Buffer.from(blockchainRID, 'hex'),
	['hello_list']
);


// Execute Query
////////////////////////////

// Execute query
gtx.query('hello_list', {})
	.then(function(result){ // Success
		console.log("--------------------------------------");
		console.log("Chromia call succeded with output:");
		console.log("--------------------------------------");
		console.log(result);
		console.log("--------------------------------------");
		console.log("Also check Chromia console in Eclipse");
		console.log("--------------------------------------");		
	})
	.catch(function(err){ // Fail
		console.log("--------------------------------------");
		console.log("Chromia call failed with output:");
		console.log("--------------------------------------");
		console.log(err);
		console.log("--------------------------------------");
		console.log("Also check Chromia console in Eclipse");
		console.log("--------------------------------------");
	});